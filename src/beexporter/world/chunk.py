from beexporter.world.block import Block
from math import floor
import numpy

def get_chunk_pos(x, y, z):
    return int(floor(float(x)/16.0)), int(floor(float(y)/16.0)), int(floor(float(z)/16.0))


def get_chunk_block_pos(x, y, z):
    return int(x % 16), int(y % 16), int(z % 16)


class Chunk:
    def __init__(self, world, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.palette = []
        self.blocks = []
        for section in world.worldObj.get_nbt(x, z)["Level"]["Sections"]:
            if int(section["Y"].value) == y:
                if "Palette" in section:
                    for item in section["Palette"]:
                        props = {}
                        if "Properties" in item:
                            for prop in item["Properties"]:
                                props[prop] = str(item["Properties"][prop].value)
                        self.palette.append(Block(item["Name"].value, props))
                    if len(section['BlockStates'].value) > 0:  # Convert the array to split the block ids
                        long_array = numpy.array(section['BlockStates'].value, dtype=">q")
                        bits_per_block = (len(long_array) * 64) // 4096
                        binary_blocks = numpy.unpackbits(long_array[::-1].astype(">i8").view("uint8"))\
                            .reshape(-1, bits_per_block)
                        self.blocks = binary_blocks.dot(2 ** numpy.arange(binary_blocks.shape[1] - 1, -1, -1))[
                                      ::-1  # Undo the bit-shifting that Minecraft does with the palette indices
                                        ][:4096]

    def get_block(self, x, y, z):
        if len(self.blocks) == 0:
            return Block("minecraft:air")
        block_pos = y*16*16 + z*16 + x
        block_num = self.blocks[block_pos]
        return self.palette[block_num]
