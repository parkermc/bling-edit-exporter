import traceback

from PyQt5.QtCore import QThread, pyqtSignal
from beexporter.world import World


class OpenWorker(QThread):
    error = pyqtSignal(['QString'])
    done = pyqtSignal()

    def __init__(self, world_folder):
        QThread.__init__(self)
        self.world_folder = world_folder
        self.world = World(world_folder)

    def __del__(self):
        self.wait()

    def run(self):
        try:
            self.world.load()
            self.done.emit()
        except Exception as e:
            print(e)
            traceback.print_exc()
            self.error.emit(str(e))
