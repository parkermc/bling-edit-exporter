import zipfile, traceback, re

from PyQt5.QtCore import QThread, pyqtSignal
from beexporter.workers import export_files
from beexporter.world import World, get_chunk_pos, get_chunk_block_pos


class ExportWorker(QThread):
    update_progress = pyqtSignal(int)
    error = pyqtSignal(['QString'])
    done = pyqtSignal()

    def __init__(self, world, name, namespace, out_file, export_blocks, export_air, blocks_per_tick):
        QThread.__init__(self)
        self.world = world
        self.name = name
        self.namespace = namespace
        self.out_file = out_file
        self.export_blocks = export_blocks
        self.export_air = export_air
        self.blocks_per_tick = blocks_per_tick
        self.size = (int(1 + self.world.max_pos[0] - self.world.min_pos[0]),
                     int(1 + self.world.max_pos[1] - self.world.min_pos[1]),
                     int(1 + self.world.max_pos[2] - self.world.min_pos[2]))

    def __del__(self):
        self.wait()

    def fully_split(self, reg, text):
        out = [text]
        while re.search(reg, out[-1], re.MULTILINE):
            add = re.split(reg, out[-1], re.MULTILINE)
            out.pop()
            out.extend(add)
        return out

    def parse(self, filename, text, data={}):
        try:
            split_text = self.fully_split(r"{{([\s\S]*?)}}", text)
            out = ""
            remove_new_line = False
            skip = 0
            for i in range(0, len(split_text)):
                if skip == 0:
                    if i % 2 == 0:
                        if remove_new_line:
                            if split_text[i][:1] == "\n":
                                out += split_text[i][1:]
                            remove_new_line = False
                        else:
                            out += split_text[i]
                    else:
                        if split_text[i][:4].lower() == "for " or split_text[i][:3].lower() == "if ":
                            statement_code = ""
                            statements = 0
                            exit_line = 0
                            # Get code to run
                            for j in range(i + 1, len(split_text)):
                                if j % 2 == 0:
                                    if j == i + 1 and split_text[j][:1] == "\n":
                                        statement_code += split_text[j][1:]
                                    else:
                                        statement_code += split_text[j]
                                else:
                                    if split_text[j][:4].lower() == "for " or split_text[j][:3].lower() == "if ":
                                        statements += 1
                                    elif split_text[j][:3].lower() == "end":
                                        statements -= 1
                                        if statements == -1:
                                            exit_line = j
                                            break
                                    statement_code += "{{" + split_text[j] + "}}"
                            if exit_line == 0:
                                raise Exception("a statement on line " + str(i + 1) + " never ended")
                            if split_text[i][:3].lower() == "for":
                                prams_arr = split_text[i][3:].split(" in ")
                                if len(prams_arr) != 2:
                                    raise Exception("for loop syntax error!")
                                loop_arr = eval("list(" + prams_arr[1] + ")")
                                skip = exit_line - i
                                for j in loop_arr:
                                    data[prams_arr[0].strip()] = j
                                    out += self.parse("", statement_code, data)
                                if (out[-1:] == "\n" or out == "") and split_text[exit_line + 1][:1] == "\n":
                                    remove_new_line = True
                            else:
                                else_found = False
                                true_code = ""
                                false_code = ""
                                statements = 0
                                switch_line = -1
                                split_code = self.fully_split(r"{{([\s\S]*?)}}", statement_code)
                                for j in range(0, len(split_code)):
                                    if j % 2 == 0:
                                        if else_found:
                                            if j == switch_line + 1 and split_code[j][:1] == "\n":
                                                false_code += split_code[j][1:]
                                            else:
                                                false_code += split_code[j]
                                        else:
                                            true_code += split_code[j]
                                    else:
                                        if split_code[j][:4].lower() == "for " or split_code[j][:3].lower() == "if ":
                                            statements += 1
                                        elif split_code[j][:3].lower() == "end":
                                            statements -= 1
                                        elif split_code[j][:4].lower() == "else":
                                            switch_line = j
                                            else_found = True
                                            continue
                                        if else_found:
                                            false_code += "{{" + split_code[j] + "}}"
                                        else:
                                            true_code += "{{" + split_code[j] + "}}"
                                run = eval("bool(" + split_text[i][3:] + ")")
                                skip = exit_line - i
                                if run:
                                    out += self.parse("", true_code, data)
                                else:
                                    out += self.parse("", false_code, data)
                                if (out[-1:] == "\n" or out == "") and split_text[exit_line + 1][:1] == "\n":
                                    remove_new_line = True
                        else:
                            try:
                                out += str(eval(split_text[i]))
                            except SyntaxError:
                                exec(split_text[i])
                                if (out[-1:] == "\n" or out == "") and split_text[i + 1][:1] == "\n":
                                    remove_new_line = True
                else:
                    skip -= 1
            return out
        except Exception as e:
            if filename != "":
                raise type(e)(str(e) + " happens in %s" % filename)
            else:
                raise e

    def run(self):
        try:
            self.update_progress.emit(0)
            blocks_per_percent = int(((1 + self.world.max_pos[0] - self.world.min_pos[0]) *
                                (1 + self.world.max_pos[1] - self.world.min_pos[1]) *
                                (1 + self.world.max_pos[2] - self.world.min_pos[2])) / 100)
            blocks_per_update = blocks_per_percent
            if blocks_per_update <= 100:
                blocks_per_update = 100
            block_count = 0
            files_used = 0
            file_line_count = 0
            file_text = ""
            chunks = {}
            zipf = zipfile.ZipFile(self.out_file, "w", zipfile.ZIP_DEFLATED)
            for x in range(self.world.min_pos[0], self.world.max_pos[0] + 1):
                for y in range(self.world.min_pos[1], self.world.max_pos[1] + 1):
                    for z in range(self.world.min_pos[2], self.world.max_pos[2] + 1):
                        block_count += 1
                        if block_count % blocks_per_update == 0:
                            self.update_progress.emit(block_count/blocks_per_percent)
                        chunk_pos = get_chunk_pos(x, y, z)
                        if chunk_pos not in chunks.keys():
                            chunks[chunk_pos] = self.world.get_chunk(chunk_pos[0], chunk_pos[1], chunk_pos[2])
                        block_pos = get_chunk_block_pos(x, y, z)
                        block = chunks[chunk_pos].get_block(block_pos[0], block_pos[1], block_pos[2])
                        is_air = False
                        if block.name == "minecraft:air" or block.name == "minecraft:void_air" or \
                                block.name == "minecraft:cave_air":
                            is_air = True
                        if(is_air and self.export_air) or ((not is_air) and self.export_blocks):
                            if file_line_count == self.blocks_per_tick:
                                zipf.writestr("data/blingedit_i_" + self.namespace + "/functions/import_" + str(files_used) +
                                              ".mcfunction", file_text)
                                files_used += 1
                                file_line_count = 0
                                file_text = ""
                            file_line_count += 1
                            file_text += "setblock ~" +\
                                         str(x - self.world.min_pos[0]) + " ~" + str(y - self.world.min_pos[1]) +\
                                         " ~" + str(z - self.world.min_pos[2]) + " " + block.name
                            first = True
                            for prop in block.states:
                                if first:
                                    file_text += "[" + prop + "=" + block.states[prop]
                                    first = False
                                else:
                                    file_text += "," + prop + "=" + block.states[prop]
                            if not first:
                                file_text += "]\n"
                            else:
                                file_text += "\n"
            zipf.writestr("data/blingedit_i_" + self.namespace + "/functions/import_" + str(files_used) + ".mcfunction", file_text)
            file_per_percent = files_used / 100.0 # Prefer this but use percent_per_file if this is less than one
            percent_per_file = 100.0 / (files_used + 1)
            last_percent = -1
            file_text = ""
            for i in range(0, files_used + 1):
                if file_per_percent >= 1:
                    if int(i/file_per_percent) != last_percent:
                        file_text += "execute if entity @e[type=armor_stand,tag=import,name=i_"+self.namespace+",sort=nearest,limit=1,scores={bee_age="+str(i)+"}] run bossbar set bee_progress value " + str(int(i/file_per_percent))+"\n"
                        last_percent = int(i/file_per_percent)
                else: # can't be the same as it is less than one file per percent
                    file_text += "execute if entity @e[type=armor_stand,tag=import,name=i_"+self.namespace+",sort=nearest,limit=1,scores={bee_age="+str(i)+"}] run bossbar set bee_progress value "+str(int(percent_per_file*i))+"\n"
                file_text += "execute if entity @e[type=armor_stand,tag=import,name=i_"+self.namespace+",sort=nearest,limit=1,scores={bee_age="+str(i)+"}] run function blingedit_i_"+self.namespace+":import_"+str(i)+"\n"
            file_text += "scoreboard players add @e[type=armor_stand,tag=import,name=i_"+self.namespace+",sort=nearest,limit=1] bee_age 1\n"
            file_text += "execute if entity @e[type=armor_stand,tag=import,name=i_"+self.namespace+",sort=nearest,limit=1,scores={bee_age="+str(files_used)+"}] run bossbar set bee_progress players\n"
            file_text += "kill @e[type=armor_stand,tag=import,name=i_"+self.namespace+",scores={bee_age="+str(files_used)+"..}]\n"
            zipf.writestr("data/blingedit_i_" + self.namespace + "/functions/do_import.mcfunction", file_text)

            for filename in export_files.files:
                zipf.writestr(self.parse(filename, filename), self.parse(filename, export_files.files[filename]))
            chunks.clear()
            zipf.close()
            self.done.emit()
        except Exception as e:
            print(e)
            traceback.print_exc()
            self.error.emit(str(e))
