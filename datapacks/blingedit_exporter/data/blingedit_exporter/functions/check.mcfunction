# Check if BlingEdit is ready to run the plugin
function blingedit:plugin_can_run
# Then run it
execute if score Global plugin_can_run matches 1.. run function blingedit_exporter:export_area
# Error message if no region was selected by the user
execute unless score Global plugin_can_run matches 1.. run tellraw @s ["",{"text":"\nYou haven't selected a region yet\n","color":"red"}]

gamerule sendCommandFeedback false
