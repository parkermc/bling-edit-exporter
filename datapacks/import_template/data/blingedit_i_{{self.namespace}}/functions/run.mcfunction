#scoreboard players set Global dx 10
#scoreboard players set Global dy 10
#scoreboard players set Global dz 10

scoreboard players add Global _unique 1
execute unless score @s _id matches 0.. run scoreboard players operation @s _id = Global _unique
scoreboard players operation Global player = @s _id
execute as @s run function blingedit:get_cursor_position
scoreboard players set Global bottom_center 0

{{for i in [int(-self.size[0] * 500), 0, int(self.size[0] * 500)]}}
scoreboard players set Global _dpos_0 {{data["i"]}}
scoreboard players set Global _dpos_1 -{{str(int(self.size[1] * 500))}}
scoreboard players set Global _dpos_2 -{{str(int(self.size[2] * 500))}}
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_1 0
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_1 {{str(int(self.size[1] * 500))}}
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_2 0
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_2 {{str(int(self.size[2] * 500))}}
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_1 0
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_1 -{{str(int(self.size[1] * 500))}}
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_2 0
{{if data["i"] == 0}}
scoreboard players set Global bottom_center 1
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global bottom_center 0
{{else}}
function blingedit_i_{{self.namespace}}:corner/create
scoreboard players set Global _dpos_1 0
function blingedit_i_{{self.namespace}}:corner/create
{{end}}
{{end}}

execute as @e[type=minecraft:magma_cube,tag=Corner] at @s run tp @s ~ ~ ~
scoreboard players operation @s moving = Global moving
scoreboard players set @s state 9
execute as @s run function blingedit:create_click
scoreboard players set @s clone_rotation 0
scoreboard players set @s clone_flip_x 0
scoreboard players set @s clone_flip_z 0
tellraw @s [""]
tellraw @s ["",{"text":"Move "},{"text":"Dest","color":"blue"},{"text":": ","color":"gray"},{"text":"[Up]","clickEvent":{"action":"run_command","value":"/function blingedit:move_up"},"color":"aqua"},{"text":" ","color":"aqua"},{"text":"[Down]","clickEvent":{"action":"run_command","value":"/function blingedit:move_down"},"color":"aqua"},{"text":" ","color":"aqua"},{"text":"[Left]","clickEvent":{"action":"run_command","value":"/function blingedit:move_left"},"color":"aqua"},{"text":" ","color":"aqua"},{"text":"[Right]","clickEvent":{"action":"run_command","value":"/function blingedit:move_right"},"color":"aqua"},{"text":" ","color":"aqua"},{"text":"[Forward]","clickEvent":{"action":"run_command","value":"/function blingedit:move_forward"},"color":"aqua"},{"text":" ","color":"aqua"},{"text":"[Backward]","clickEvent":{"action":"run_command","value":"/function blingedit:move_backward"},"color":"aqua"}]
tellraw @s ["",{"text":"Actions: "},{"text":"[Confirm]","clickEvent":{"action":"run_command","value":"/function blingedit_i_{{self.namespace}}:start_import"},"color":"green"},{"text":" ","color":"aqua"},{"text":"[Cancel]","clickEvent":{"action":"run_command","value":"/function blingedit:cancel"},"color":"green"}]


