.PHONY: help develop devpython build resources ui clean

RESOURCE_DIR = src/resources
COMPILED_DIR = src/beexporter/ui

COMPILED_UI = $(UI_FILES:%.ui=$(COMPILED_DIR)/ui_%.py)
COMPILED_RESOURCES = $(RESOURCES:%.qrc=$(COMPILED_DIR)/%_rc.py)
UI_FILES = main.ui about.ui
RESOURCES = resources.qrc
PYUIC = pyuic5
PYRCC = pyrcc5


help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed 

build: devpython ui
	python setup.py install
	pip install pyinstaller
	pyinstaller --icon=src/resources/icon.ico -w --onefile src/beexporter/main.py

devpython:
	pip install -r requirements.txt
	python src/beexporter/tools/generate_export_files.py

develop: devpython resources ui
	python setup.py develop

resources : $(COMPILED_RESOURCES)

ui : $(COMPILED_UI)

$(COMPILED_DIR)/%_rc.py : $(RESOURCE_DIR)/%.qrc
	$(PYRCC) $< -o $@
 
$(COMPILED_DIR)/ui_%.py : $(RESOURCE_DIR)/%.ui
	$(PYUIC) --import-from=beexporter.ui $< -o $@
 
clean : 
	$(RM) $(COMPILED_UI) $(COMPILED_RESOURCES) $(COMPILED_UI:.py=.pyc) $(COMPILED_RESOURCES:.py=.pyc)  
